# square root function
def root_method_1(max: int = 1):
    roots = []
    for i in range(1, max+1):
        roots.append((i, i ** 2))
    return roots

def root_method_2(max: int = 1):
    return [(i, i ** 2) for i in range(1, max+1)]

# loop and get input unitl the user types quits
while True:
    try:
        user_input = input("> ")
        if user_input == "quit":
            break
        # convert the input to an int
        user_input = int(user_input)
        print(f"Method 1: {root_method_1(user_input)}")
        print(f"Method 2: {root_method_2(user_input)}")
    except ValueError:
        print("Please enter a number")

    except KeyboardInterrupt:
        break