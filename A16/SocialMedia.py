# only going to be the classes because im gonig to expand on them in A16

class User:
    """
    A class that defines a user.
    """
    # constructor
    def __init__(self, name, password):
        """
        Constructor for the User class.
        """
        self.name = name
        self.password = password
        self.friends = []

    # add friend
    def add_friend(self, friend):
        """
        Add a friend to the user.
        """
        if friend not in self.friends:
            self.friends.append(friend)
            return (f"{friend} added.")
        else:
            return (f"{friend} already exists.")

    # remove friend
    def remove_friend(self, friend):
        """
        Remove a friend from the user.
        """
        if friend in self.friends:
            self.friends.remove(friend)
            return (f"{friend} removed.")
        else:
            return (f"{friend} does not exist.")

    # show friends
    def show_friends(self):
        """
        Show the user's friends.
        """
        if len(self.friends) > 0:
            n = ""
            for friend in self.friends:
                n += f"{friend}\n"
            return (f"{self.name}'s friends:\n{n}")
        else:
            return (f"{self.name} has no friends.")

    # get name
    def get_name(self):
        """
        Get the user's name.
        """
        return (self.name)

    # get password
    def get_password(self):
        """
        Get the user's password.
        """
        return (self.password)

    # get friends
    def get_friends(self):
        """
        Get the user's friends.
        """
        return (self.friends)

    # set name
    def set_name(self, name):
        """
        Set the user's name.
        """
        self.name = name
        return (f"Name set to {self.name}.")

    # set password
    def set_password(self, password):
        """
        Set the user's password.
        """
        self.password = password
        return (f"Password set to {self.password}.")

    # set friends
    def set_friends(self, friends):
        """
        Set the user's friends.
        """
        self.friends = friends
        return (f"Friends set to {self.friends}.")

class UserManager:

    """
    A class that defines a user manager.
    """
    # constructor
    def __init__(self):
        """
        Constructor for the UserManager class.
        """
        self.users = []

    # add user
    def add_user(self, user):
        """
        Add a user to the user manager.
        """
        if user not in self.users:
            self.users.append(user)
            return (f"{user} added.")
        else:
            return (f"{user} already exists.")

    # remove user
    def remove_user(self, user):
        """
        Remove a user from the user manager.
        """
        if user in self.users:
            self.users.remove(user)
            return (f"{user} removed.")
        else:
            return (f"{user} does not exist.")

    # show users
    def show_users(self):
        """
        Show the users in the user manager.
        """
        if len(self.users) > 0:
            n = ""
            for user in self.users:
                n += f"{user}\n"
            return (f"{self.name}'s users:\n{n}")
        else:
            return (f"{self.name} has no users.")

    # get users
    def get_users(self):
        """
        Get the users in the user manager.
        """
        return (self.users)

    # set users
    def set_users(self, users):
        """
        Set the users in the user manager.
        """
        self.users = users
        return (f"Users set to {self.users}.")
