

while True:
    try:
        # Prompt the user to enter a grade
        print("Enter A Grade (int) or \"quit\" to cancel")
        i = input(">")
        
        # If the user enters "quit", exit the program
        if i == 'quit':
            exit(0)
        else:
            # Convert the user input to an integer
            grade = int(i)

        # If the user input is between 0 and 100, break out of the loop
        if 0 <= grade <= 100:
            break
        else:
            # If the user input is not between 0 and 100, raise a ValueError
            raise ValueError()

    # If the user enters an invalid input, print an error message
    except ValueError:
        print("Invalid Input")

if grade >= 90:
    # If grade is greater than or equal to 90, print "Excellent"
    print("Excellent")
elif 70 <=  grade < 90:
    # If grade is between 70 and 90, print "Good"
    print("Good")
elif 50 <=  grade < 70:
    # If grade is between 50 and 70, print "Not Bad"
    print("Not Bad")
elif 40 <=  grade < 50:
    # If grade is between 40 and 50, print "I think we'd better talk"
    print("I think we'd better talk")
else:
    # If grade is less than 40, print "Oh dear!"
    print("Oh dear!")