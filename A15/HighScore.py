import json
import os
import sys

import inspect
import math
# im going to be reusing the Calculator class from A12/Calculator.py because it has a lot of useful methods and i dont want to rewrite them all
# it also has a good cli interface that i can reuse for this program

class HighScoreManager():
    def __init__(self):
        self.ignore = ["__init__", "commands", "run"]
        self.highscores = {}
        pass

    def commands(self):
        # create a dict of all the commands
        commands = {}
        # get all the methods in the class
        methods = inspect.getmembers(self, predicate=inspect.ismethod)
        # loop through all the methods
        for method in methods:
            # skip the methods in the ignore list
            if method[0] in self.ignore: continue
            # get the docs for the method
            docs = inspect.getdoc(method[1])
            # get the argspec for the method
            argspec = inspect.getfullargspec(method[1])
            # get the annotations for the method
            annotations = argspec.annotations
            # get the args from argspec
            args = argspec.args
            # remove self from the args
            args.remove("self")
            # check if the method has any docs
            if docs != None:
                # add the method to the commands dict
                commands[method[0]] = {
                    "docs": docs,
                    "args": args,
                    "annotations": annotations
                }
        # set "commands_dict" to commands 
        self.commands_dict = commands
        # return the commands
        return commands
    def run(self,command: str):
        # strip the command of any spaces at the start or end
        command = command.strip()
        # split the command into a list
        command = command.split(" ")
        # check if the first element of the command matches any of the commands in the commands dict
        if command[0] in self.commands_dict:

            # check if the command has the correct number of arguments
            if len(command) - 1 == len(self.commands_dict[command[0]]["args"]):
                # catch math errors
                try:
                    # check if the command has any annotations
                    if len(self.commands_dict[command[0]]["annotations"]) > 0:
                        # create a list of the annotations
                        annotations = list(self.commands_dict[command[0]]["annotations"].values())
                        # create a list of the arguments
                        args = command[1:]
                        # create a list of the arguments and annotations
                        args = list(zip(args, annotations))
                        # create a list of the arguments and annotations that have been converted to the correct type
                        args = [arg[1](arg[0]) for arg in args]
                        # call the command and pass the arguments
                        print(getattr(self, command[0])(*args))
                    else:
                        # call the command and pass the arguments
                        print(getattr(self, command[0])(*command[1:]))
                except ValueError as e:
                    # print an error message
                    print(f"Error: {e}")
                # catch ZeroDivisionError
                except ZeroDivisionError as e:
                    # print an error message
                    print(f"Error: {e}")
            else:
                # print an error message
                print(f"Command \"{command[0]}\" takes {len(self.commands_dict[command[0]]['args'])} arguments, {len(command) - 1} given.")
        else:
            # print an error message
            print(f"Error: Command \"{command[0]}\" not found.")


    # -------------------------------------------------------------------------# COMMANDS BELOW
    # help
    def help(self):
        """
        Print the help message.
        """
        print("Commands:")
        for command in self.commands_dict:
            print(f"  {command}: {self.commands_dict[command]['docs']}")
        return ""
    # quit
    def quit(self):
        """
        Quit the calculator.
        """
        print("Quitting...")
        exit(0)

    # read highscores
    def read(self, file):
        """
        Read the highscores from a file.
        """
        # check if the file exists
        if os.path.exists(file):
            # read the file
            with open(file, "r") as file:
                # load the json
                self.highscores = json.load(file)
                return ("File read.")
        else:
            # print an error message
            return(f"Error: No file found at \"{file}\"")
    # write highscores
    def write(self, file):
        """
        Write the highscores to a file.
        """
        # check if the file exists if it does prompt the user to overwrite it or not
        if os.path.exists(file):
            # get the user input
            user_input = input(f"File \"{file}\" already exists, overwrite? (y/n) ")
            # check if the user input is yes
            if user_input.lower() == "y":
                # write the file
                with open(file, "w") as file:
                    # dump the json
                    json.dump(self.highscores, file, indent=4)
                    self.highscores = {}
                    return ("File written.")
            else:
                # print an error message
                print("Error: File not written.")
        else:
            # write the file
            with open(file, "w") as file:
                # dump the json
                json.dump(self.highscores, file, indent=4)
                self.highscores = {}
                return("File written.")

    # show highscores
    def show(self):
        """
        Show the highscores.
        """
        # check if there are any highscores
        if len(self.highscores) > 0:
            n = ""
            # print the highscores in a table format
            for index, (name, score) in enumerate(self.highscores.items()):
                n += f"{index}. {name}: {score}\n"

            return n
        else:
            # print an error message
            return("Error: No highscores found.")

    # add highscore
    def add(self, name: str, score: int):
        """
        Add a highscore.
        """
        # check if the name is a string
        if type(name) == str:
            # check if the score is an int
            if type(score) == int:
                # add the highscore
                self.highscores[name] = score
                return("Highscore added.")
            else:
                # print an error message
                return("Error: Score must be an integer.") 
            # sort the highscores
            self.highscores = dict(sorted(self.highscores.items(), key=lambda item: item[1], reverse=False))
        else:
            # print an error message
            return("Error: Name must be a string.")
    # remove highscore
    def remove(self, name: str):
        """
        Remove a highscore.
        """
        # check if the name is a string
        if type(name) == str:
            # check if the name is in the highscores
            if name in self.highscores:
                # remove the highscore
                del self.highscores[name]
                return("Highscore removed.")
            else:
                # print an error message
                return("Error: Name not found.")
            self.highscores = dict(sorted(self.highscores.items(), key=lambda item: item[1], reverse=False))
        else:
            # print an error message
            return("Error: Name must be a string.")
    # clear highscores
    def clear(self):
        """
        Clear the highscores.
        """
        # clear the highscores
        self.highscores = {}
        return("Highscores cleared.")
    # -------------------------------------------------------------------------# COMMANDS ABOVE 

def loop():

    # create a new instance of the HighScoreManager class
    calc = HighScoreManager()
    # get the commands
    commands = calc.commands()
    # loop forever
    while True:
        # get the command from the user and run it
        calc.run(input("> "))


if __name__ == "__main__":
    loop()