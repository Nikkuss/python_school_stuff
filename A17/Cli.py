from A16.SocialMedia import User
from A16.SocialMedia import UserManager



class AdminUser(User):
    """
    A class that defines an admin user.
    """
    # constructor
    def __init__(self, name, password):
        """
        Constructor for the AdminUser class.
        """
        super().__init__(name, password)
        self.admin = False

    # get admin
    def get_admin(self):
        """
        Get the user's admin status.
        """
        return (self.admin)

    # set admin
    def set_admin(self, admin):
        """
        Set the user's admin status.
        """
        self.admin = admin

class AdminUserManager(UserManager):
    """
    A class that defines an admin user manager.
    """
    # constructor
    def __init__(self):
        """
        Constructor for the AdminUserManager class.
        """
        super().__init__()
        self.admin_users = []

    # get admin users
    def get_admin_users(self):
        """
        Get the admin users.
        """
        return (self.admin_users)

    # set admin users
    def set_admin_users(self, admin_users):
        """
        Set the admin users.
        """
        self.admin_users = admin_users

    # add admin user
    def add_admin_user(self, admin_user):
        """
        Add an admin user.
        """
        self.admin_users.append(admin_user)
        return

usermanager = AdminUserManager()

user = AdminUser("Admin", "test")
usermanager.add_admin_user(user)
print(usermanager.get_admin_users())
print(user.get_admin())
user.set_admin(True)
print(user.get_admin())
print(usermanager.get_admin_users())
usermanager.set_admin_users([])
print(usermanager.get_admin_users())




