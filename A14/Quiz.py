import sys
import os



def quiz_parser(data: str):
    quizs = []
    
    # split the item into a list
    data = data.split(";")
    # convert list to string
    for item in data:
        # create a dict to store the data
        data_dict = {}
        # join the list into a string
        item = ''.join(item)
        # split the item into a list
        content = item.split("|")
        # get the name of the quiz
        name = content[0]
        # get the questions and answers
        item = content[1]
        # split the item into a list
        item = item.split(":")
        # catch index errors
        try:

            # split the items into a list of 2s
            items = [item[i:i + 2] for i in range(0, len(item), 2)]
            # loop through the items
            for item in items:
                data_dict[item[0]] = item[1]
            answer = ""
            questions = []
            for key in data_dict:
                if key.startswith("a"):
                    answer = data_dict[key]
                else:
                    questions.append(data_dict[key])
            output_dict = {
                "name": name,
                "questions": questions,
                "answer": data_dict[answer]
            }
            quizs.append(output_dict)
        except IndexError:
            # print an error message
            print("Error: Invalid data")
            exit(1)
        except KeyError:
            # print an error message
            print("Error: Invalid data")
            exit(1)
    
    return quizs
    pass





# get commandline arguments
if len(sys.argv) > 1:
    arg = sys.argv[1]
else:
    print("Error: No argument given")
    exit(1)
# check if the file exists
if os.path.exists(arg):
    path = arg
else:
    print(f"Error: No file found at \"{arg}\"")
    exit(1)
# read the file
with open(path, "r") as file:
    file = file.read()
    # call the parser
    quizs = quiz_parser(file)

    # loop through the quizs
    for quiz in quizs:
        # print the name of the quiz
        print(quiz["name"])
        # loop through the questions
        for question in enumerate(quiz["questions"], start=1):
            # print the question with indentation
            print(f"{question[0]}    {question[1]}")
        tries = 0
        index_answer = quiz["questions"].index(quiz["answer"])
        # While loop to check if the answer is correct
        while True:
            # get the answer from the user
            answer = input("Answer: ")
            # check if the answer is correct
            if answer == quiz["answer"] or answer == str(index_answer + 1):
                # print a message
                print("Correct!")
                # break out of the loop
                break
            else:
                print("Incorrect!")
                if tries == 2:
                    # print a message
                    print("Failed!!!")
                    print("Answer was " + quiz["answer"])
                    # break out of the loop
                    break
                tries += 1

    print("Thanks for playing!")
