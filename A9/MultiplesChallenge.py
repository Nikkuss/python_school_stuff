

def print_first_if_mul(a, b):
    # Check if the remainder of a divided by b is equal to 0
    if a % b == 0:
        # If so, print the value of a
        print(a)


while True:
    try:

        # Prompt the user to enter a number or quit
        print("Enter A Number (int) or \"quit\" to cancel")
        i = input(">")

        # If the user enters "quit", exit the program
        if i == 'quit':
            exit(0)
        else:

            # Convert the user input to an integer
            value = int(i)

            # Iterate through the range of 1 to 100
            for i in range(1, 100):
                # Print the first number if it is a multiple of the user input
                print_first_if_mul(i, value)

    # If the user enters an invalid input
    except ValueError:
        print("Invalid Input")
