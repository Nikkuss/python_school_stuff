class Game():
    def __init__(self):
        self.board = []
        for _ in range(8):
            xx = []
            for _ in range(8):
                xx.append("*")
            self.board.append(xx)

    
    def dispaly(self):
        # a somewhat hacky way to display the board but it works
        for x in enumerate(self.board):
            print("-"*33)
            for y in enumerate(x[1]):
                if y[0] == 0:
                    print(f"| {y[1]}", end=" | ")
                else:
                    print(y[1], end=" | ")
                if y[0] == len(x[1]) - 1:
                    print()
            if x[0] == len(x[1]) -1:
                print("-"*33)

    def start(self):
        print("Starting Game")
        self.dispaly()


game = Game()
game.start()

# intentionally using a class here so i can use the same code for the bot and the player later on