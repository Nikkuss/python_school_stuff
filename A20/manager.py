import tkinter as tk
import os
from tkinter import filedialog

class GameLauncher:
    def __init__(self, master):
        self.master = master
        master.title("Game Launcher")
        
        # Create the game buttons
        self.games = [
        ]
        for i, game in enumerate(self.games):
            self.create_button(game['name'], game['filename'], game['image'], i // 2, i % 2)
        
        # Create the add game button
        self.add_game_button = tk.Button(master, text='Add Game', width=10, height=2, font=('Arial', 16), command=self.add_game)
        self.add_game_button.grid(row=len(self.games) // 2 + 1, column=0, columnspan=2, padx=5, pady=5)
        
    def create_button(self, text, filename, image, row, column):
        # Create the button image
        img = tk.PhotoImage(file=image)
        
        # Create the button
        button = tk.Button(self.master, text=text, image=img, width=100, height=100, compound='top', font=('Arial', 16), command=lambda: self.launch_game(filename))
        button.image = img
        button.grid(row=row, column=column, padx=5, pady=5)
        
    def launch_game(self, filename):
        os.system(f'\{filename}')
        
    def add_game(self):
        # Open the file dialog to select the game executable
        filename = filedialog.askopenfilename(initialdir='.', title='Select Game Executable', filetypes=[('Executable', '*')])
        if filename:
            # Open the file dialog to select the game image
            image = filedialog.askopenfilename(initialdir='.', title='Select Game Image', filetypes=[('Image Files', '*.png')])
            if image:
                # Add the game to the list of games
                name = os.path.splitext(os.path.basename(filename))[0]
                self.games.append({'name': name, 'filename': filename, 'image': image})
                
                # Create the game button
                self.create_button(name, filename, image, len(self.games) // 2, len(self.games) % 2)
                
    def change_executable(self, index):
        # Open the file dialog to select the new game executable
        filename = filedialog.askopenfilename(initialdir='.', title='Select Game Executable', filetypes=[('Executable', '*')])
        if filename:
            # Update the game executable
            self.games[index]['filename'] = filename
        
    def change_image(self, index):
        # Open the file dialog to select the new game image
        image = filedialog.askopenfilename(initialdir='.', title='Select Game Image', filetypes=[('Image Files', '*.png')])
        if image:
            # Update the game image
            self.games[index]['image'] = image
            # Update the game button image
            img = tk.PhotoImage(file=image)
            button = self.master.grid_slaves(row=index // 2, column=index % 2)[0]
            button.configure(image=img)
            button.image = img
        
root = tk.Tk()
game_launcher = GameLauncher(root)
root.mainloop()