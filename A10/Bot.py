from random import sample
import requests

NEGATIVE_WORDS = ["worst", "bad", "without"] # this would need more tweeking for better results but it works good enough for now


def clear():
    """Clear the screen."""
    print("\033[H\033[J")# this is a special character that clears the screen

def get_question():
    """Return a random question."""
    print("Generating question...")
    html = requests.get("https://www.conversationstarters.com/random.php").text # not my site but was one of the first results on google
    question = html.split(">")[1] # this is a bit of a hack but it works
    return question

# function to generate positive answers
def generate_positive(question):
    # some magic to tell if a question is positive or negative

    # check if question contains any elements of "NEGATIVE_WORDS"

    positive = not any([x in question for x in NEGATIVE_WORDS])

    if positive:
        return generate_positive_answer()
    else:
        return generate_negative_answer()


def generate_negative_answer():
    """Generate a negative answer."""
    negatives = ["How so?", "How come?", ] # this is a bit short but it works
    return sample(negatives, 1)[0]

def generate_positive_answer():
    """Generate a positive answer."""
    positives = ["That sounds great!", "Wonderfull!", "I agree!", "I think so too!", "I agree with you!", "I think that's a great idea!", ] # this could be expanded
    return sample(positives, 1)[0]




while True:
    try:
        clear()
        q = get_question()
        print(q)
        user_input = input("> ")
        if user_input == "quit":
            break
        print(generate_positive(q))
        input("Press enter to continue...")
    except KeyboardInterrupt:
        break