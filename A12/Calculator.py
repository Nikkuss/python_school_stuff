import inspect
import math
# this class is a bit long since each operation needs to be defined 
# im really happy with how this turned out
# it uses a lot of python magic to get the docs and args for each method and then uses that to generate the help message
# it also uses the annotations to convert the arguments to the correct type

class Calculator():
    def __init__(self):
        pass

    def commands(self):
        # create a dict of all the commands
        commands = {}
        # get all the methods in the class
        methods = inspect.getmembers(self, predicate=inspect.ismethod)
        # loop through all the methods
        for method in methods:
            # skip the __init__ method
            if method[0] == "__init__": continue
            # get the docs for the method
            docs = inspect.getdoc(method[1])
            # get the argspec for the method
            argspec = inspect.getfullargspec(method[1])
            # get the annotations for the method
            annotations = argspec.annotations
            # get the args from argspec
            args = argspec.args
            # remove self from the args
            args.remove("self")
            # check if the method has any docs
            if docs != None:
                # add the method to the commands dict
                commands[method[0]] = {
                    "docs": docs,
                    "args": args,
                    "annotations": annotations
                }
        # set "commands_dict" to commands 
        self.commands_dict = commands
        # return the commands
        return commands
    def run(self,command: str):
        # strip the command of any spaces at the start or end
        command = command.strip()
        # split the command into a list
        command = command.split(" ")
        # check if the first element of the command matches any of the commands in the commands dict
        if command[0] in self.commands_dict:

            # check if the command has the correct number of arguments
            if len(command) - 1 == len(self.commands_dict[command[0]]["args"]):
                # catch math errors
                try:
                    # check if the command has any annotations
                    if len(self.commands_dict[command[0]]["annotations"]) > 0:
                        # create a list of the annotations
                        annotations = list(self.commands_dict[command[0]]["annotations"].values())
                        # create a list of the arguments
                        args = command[1:]
                        # create a list of the arguments and annotations
                        args = list(zip(args, annotations))
                        # create a list of the arguments and annotations that have been converted to the correct type
                        args = [arg[1](arg[0]) for arg in args]
                        # call the command and pass the arguments
                        print(getattr(self, command[0])(*args))
                    else:
                        # call the command and pass the arguments
                        print(getattr(self, command[0])(*command[1:]))
                except ValueError as e:
                    # print an error message
                    print(f"Error: {e}")
                # catch ZeroDivisionError
                except ZeroDivisionError as e:
                    # print an error message
                    print(f"Error: {e}")
            else:
                # print an error message
                print(f"Command \"{command[0]}\" takes {len(self.commands_dict[command[0]]['args'])} arguments, {len(command) - 1} given.")



    # -------------------------------------------------------------------------# COMMANDS BELOW
    # help
    def help(self):
        """
        Print the help message.
        """
        print("Commands:")
        for command in self.commands_dict:
            print(f"  {command}: {self.commands_dict[command]['docs']}")
    # quit
    def quit(self):
        """
        Quit the calculator.
        """
        print("Quitting...")
        exit(0)


    # add 
    def add(self, a: float, b: float):
        """
        Add 2 numbers together.
        """
        return a + b

    # subtract
    def sub(self, a: float, b: float):
        """
        Subtract 2 numbers.
        """
        return a - b

    # multiply
    def mul(self, a: float, b: float):
        """
        Multiply 2 numbers together.
        """
        return a * b

    # divide
    def div(self, a: float, b: float):
        """
        Divide 2 numbers.
        """
        return a / b
    # power
    def pow(self, a: float, b: float):
        """
        Raise a number to the power of another number.
        """
        return a ** b
    # root
    def root(self, a: float, b: float):
        """
        Get the root of a number.
        """
        return a ** (1 / b)
    # factorial
    def factorial(self, a: int):
        """
        Get the factorial of a number.
        """
        return math.factorial(a)
    # log
    def log(self, a: float, b: float):
        """
        Get the log of a number.
        """
        return math.log(a, b)
    # sin
    def sin(self, a: float):
        """
        Get the sin of a number.
        """
        return math.sin(a)
    # cos
    def cos(self, a: float):
        """
        Get the cos of a number.
        """
        return math.cos(a)
    # tan
    def tan(self, a: float):
        """
        Get the tan of a number.
        """
        return math.tan(a)
    # arcsin
    def arcsin(self, a: float):
        """
        Get the arcsin of a number.
        """
        return math.asin(a)
    # arccos
    def arccos(self, a: float):
        """
        Get the arccos of a number.
        """
        return math.acos(a)
    # arctan
    def arctan(self, a: float):
        """
        Get the arctan of a number.
        """
        return math.atan(a)
    # sinh
    def sinh(self, a: float):
        """
        Get the sinh of a number.
        """
        return math.sinh(a)
    # cosh
    def cosh(self, a: float):
        """
        Get the cosh of a number.
        """
        return math.cosh(a)
    # tanh
    def tanh(self, a: float):
        """
        Get the tanh of a number.
        """
        return math.tanh(a)
    # arcsinh
    def arcsinh(self, a: float):
        """
        Get the arcsinh of a number.
        """
        return math.asinh(a)
    # arccosh
    def arccosh(self, a: float):
        """
        Get the arccosh of a number.
        """
        return math.acosh(a)
    # arctanh
    def arctanh(self, a: float):
        """
        Get the arctanh of a number.
        """
        return math.atanh(a)
    # pi
    def pi(self):
        """
        Get the value of pi.
        """
        return math.pi




def loop():
    print("Welcome to the calculator!")
    # create a new instance of the calculator class
    calc = Calculator()
    # get the commands
    commands = calc.commands()
    # loop forever
    while True:
        # get the command from the user and run it
        calc.run(input("> "))


if __name__ == "__main__":
    loop()