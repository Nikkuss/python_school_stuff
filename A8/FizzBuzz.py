import time
#Fizz Buzz from 0 to 100
for i in range(1, 100):
    #Check if the current number is divisible by 3 and 5
    if i% 15 == 0:
        #Print "FizzBuzz" if the number is divisible by 3 and 5
        print("FizzBuzz")
    #Check if the current number is divisible by 5
    elif i% 5 == 0:
        #Print "Buzz" if the number is divisible by 5
        print("Buzz")
    #Check if the current number is divisible by 3 
    elif i% 3 == 0:
        #Print "Fizz" if the number is divisible by 3
        print("Fizz")
    #Otherwise, print the number
    else:
        print(i)
    # sleep for 0.1 seconds to show output
    time.sleep(0.1)