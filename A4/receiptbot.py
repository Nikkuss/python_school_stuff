from os import system, name, get_terminal_size
import time
width = get_terminal_size().columns

commands = [
    {
        "type": "add",
        "args": [
            {
                "type": "text",
                "value": "add"
            },
            {
                "type": "type",
                "value": "str"
            },
            {
                "type": "type",
                "value": "int"
            }
        ]
    },
        {
        "type": "remove",
        "args": [
            {
                "type": "text",
                "value": "remove"
            },
            {
                "type": "type",
                "value": "int"
            }
        ]
    },
    {
        "type": "help",
        "args": [
            {
                "type": "text",
                "value": "help"
            }
        ]
    },
        {
        "type": "print",
        "args": [
            {
                "type": "text",
                "value": "print"
            }
        ]
    }
]

# classes

class Item():
    def __init__(self, name:str, price:int) -> None:
        self.name = name
        self.price = price
    def display(self, maxp, maxn, n) -> str:
        blank = width - len(name) - maxp - maxn - 1 - len(str(n)) - 7
        return f"| {str(n).ljust(4)} | {self.name.ljust(maxn)}|{''.ljust(blank)}| {str(self.price).ljust(maxp)} |"

class ItemList():
    def __init__(self):
        self.items = []
        self.maxp = 6
        self.maxn = 5
    def calc_maxn_maxp(self):
        self.maxp = 6
        self.maxn = 5
        for item in self.items:
            if len(str(item.price)) > self.maxp:
                self.maxp = len(str(item.price))
            if len(str(item.name)) > self.maxn - 1:
                self.maxn = len(str(item.name)) + 1
    def append(self, item:Item):
        self.items.append(item)
    def pop(self, n:str):
        self.items.pop(n)
    def display(self, done=False):
        self.calc_maxn_maxp()
        blank = width - len("Name") - self.maxp -self.maxn - len('Price') - len("ID") - 3
        print("-"*width)
        print(f"| ID   | {'Name'.ljust(self.maxn)}|{''.ljust(blank)}| Price {''.ljust(self.maxp-5)}|")
        print("-"*width)
        for index, item in enumerate(self.items):
            print(item.display(self.maxp,self.maxn,index))
        if len(self.items) > 0:
            print("-"*width)
            if done:
                pass
# functions

def clear():
    # for windows
    if name == 'nt':
        system('cls')
    # for mac and linux
    else:
        system('clear')

# >help
# >add
# >remove
# >print
def get_input():
    while True:
        text = input("> ")
        data = text.split(" ")
        command_matches = []
        for command in commands:
            if len(data) != len(command['args']):
                continue
            match_arg = []
            for index, arg in enumerate(command['args']):
                if arg['type'] == 'text':
                    if not arg['value'] == data[index]:
                        match_arg.append(False)
                        continue
                    match_arg.append(True)
                if arg['type'] == 'type':
                    if arg['value'] == "int":
                        if not data[index].isnumeric():
                            match_arg.append(False)
                            continue
                        match_arg.append(True)
                    if arg['value'] == "str":
                        match_arg.append(True)
            if all(match_arg):
                return data
        print(">> Command not found")

items = ItemList()

def mainloop():
    while True:
        clear() # clear screen
        print("Items:") 
        items.display()
        action = get_input()
        if action[0] == 'add':
            items.append(Item(action[1],int(action[2])))
        elif action[0] == 'remove':
            items.pop(int(action[1]))
        elif action[0] == 'help':
            clear()
            print("> add (str) (int)\nadd an item to the menu")
            print("> remove (int)\nremove an item from the menu")
            print("> print \nexit and print receipt")
            input("...")
        elif action[0] == 'print':
            clear()
            items.display()
            break
mainloop()
