import os
import re
import sys

import jinja2
import pdfkit
import urllib3
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import get_lexer_by_name


def py2html(code, options):
    formatter = HtmlFormatter(style='vs',
                              linenos=False,
                              noclasses=True,
                              cssclass='',
                              cssstyles='width:100vw',
                              prestyles='margin: 0')
    html = highlight(code, get_lexer_by_name('python', **options), formatter)
    return html
# This code takes a file path as an argument and returns the absolute path of the file. If the file path is relative, it is converted to an absolute path.




f = sys.argv[1]
p = ""
if os.path.isabs(f):
    p = f
else:
    p = os.path.abspath(os.path.join("../", f))


f = sys.argv[1]
p = ""
if os.path.isabs(f):
    p = f
else:
    p = os.path.abspath(os.path.join("../", f))
print(f"Using {p} as path")
with open(p) as f:
    print("Reading")
    code = f.read()
    print(f"Converting to html")
    html = py2html(code, {})
    token = os.environ['TOKEN']

    pa = os.path.join("../", p.split('/')[-2:][0], f"{p.split('/')[-2:][0]}.gif")


    # response = requests.post('https://files.nikkuss.com/api/files/add', headers=headers, files=files, data=data)
    # print(response.request.headers)
    # print(response.request.body)
    # print(response.text)
    datagen, headers2 = urllib3.encode_multipart_formdata({"file": (f"{p.split('/')[-2:][0]}.gif", open(pa, 'rb').read()), 'allowedDownloads': '0', 'expiryDays': '0'})
    headers = {
        'apikey': token, 
        'Content-Type': headers2
    }
    


    #
    # upload the gif to nikkuss.com




    print("Upload Gif")
    req = urllib3.request("POST", "https://files.nikkuss.com/api/files/add", headers=headers, body=datagen)
    js = req.json()
    print(js)
    if js['Result'] != 'OK':
        print("Upload Failed exit.")
        exit(1)
    print(f"Loading template")
    context = {'html': html, 'gitfilepath': '/'.join(p.split('/')[-2:]), 'filesnikkusspath': f"{js['HotlinkUrl']}{js['FileInfo']['HotlinkId']}", 'gitvideopath': os.path.join(p.split('/')[-2:][0], f"{p.split('/')[-2:][0]}.gif")}
    template_loader = jinja2.FileSystemLoader('./')
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template('template.html')
    print(f"Rendering")
    output_text = template.render(context)
    print("Generating PDF")
    config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
    pdfkit.from_string(output_text, f"{p.split('/')[-2:][0]}_python.pdf", configuration=config, css='style.css')
    print("Done!")