import os
import json
import shutil
import re
# function that cleans the gifs folder
def clean_gifs(paths, gifs, scripts):
    # get the absolute path of the gifs folder
    gifs_abs = os.path.abspath("../gifs")
    # change the current working directory to the gifs folder
    os.chdir(gifs_abs)
    # get all the files in the gifs folder
    files_gif = os.listdir()
    # get the absolute path of the scripts folder
    scripts_abs = os.path.abspath("../scripts")
    # change the current working directory to the scripts folder
    os.chdir(scripts_abs)
    # get all the files in the scripts folder
    files_scripts = os.listdir()
    # change the current working directory to the parent directory
    os.chdir("../")
    # create new list of gifs
    new_gifs = []   
    new_scripts = []
    # loop though all the scripts in the manifest
    for script in scripts:
        # print the script that is being checked
        print(f"Checking {script['uuid']} -", end=" ")
        #join script with the script folder
        script_path = script['path']

        # get the absolute path of the script
        script_abs = os.path.abspath(script_path)
        # delete the script if it doesent exist or forceclean is true
        if not (not os.path.exists(script_abs) or forceclean):
            # print Skipping if it exists
            print("Skipping")
            # if it does append it to the new scripts list
            new_scripts.append(script)

        else:
            # print Cleaning if it doesent exist
            print("Cleaning")

    # loop though all the gifs in the manifest
    for gif in gifs:
        # print the gif that is being checked
        print(f"Checking {gif['uuid']} -", end=" ")
        # join gif with the gifs folder
        gif_path =  gif['path']
        # get the absolute path of the gif
        gif_abs = os.path.abspath(gif_path)
        # delete the gif if it doesent exist or forceclean is true
        if not (not os.path.exists(gif_abs) or forceclean):
            # print Skipping if it exists
            print("Skipping")
            # if it does append it to the new gifs list
            new_gifs.append(gif)
        else:
            # print Cleaning if it doesent exist
            print("Cleaning")


    # combine files_gif and files_scripts while adding subfolders
    files = [os.path.join(gifs_abs, file) for file in files_gif] + [os.path.join(scripts_abs, file) for file in files_scripts]
    # combine new_gifs and new_scripts
    paths = [gif['path'] for gif in new_gifs] + [script['path'] for script in new_scripts]
    # convert paths to absolute paths
    paths = [os.path.abspath(path) for path in paths]
    # loop though all the files
    for file in files:
        # print the filename of the file that is being checked 
        print(f"Checking {os.path.basename(file)} -", end=" ")
        # check if the file is not in the paths list or forceclean is true
        if (file not in paths or forceclean):
            # print Deleting if it doesent exist
            print("Deleting")
            # delete the file
            os.remove(file)
        else:
            # print Skipping if it exists
            print("Skipping")

    # return the new gifs and new scripts
    return new_gifs, new_scripts

# parse the first argument as a boolean if it exists else set it to false
forceclean = bool(os.sys.argv[1]) if len(os.sys.argv) > 1 else False

# print "Force cleaning All Files" if forceclean is true
if forceclean:
    print("Force Cleaning All Files")

# check current working directory is the pdf folder 
if os.path.basename(os.getcwd()) != "pdf":
    # change current working directory to the pdf folder
    os.chdir("pdf")

# get all files in the pdf folder
files = os.listdir()
# check if any folders look like a uuid using regex and if it does delete it using shutil
for file in files:
    # check if the file is a folder
    if os.path.isdir(file):
        # print the file that is being checked
        print(f"Checking {file} -", end=" ")
        # check if the file is a uuid using regex
        if re.match(r'^([a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8})$', file):
            # print Deleting if it is a uuid
            print("Deleting")
            # delete the file
            shutil.rmtree(file)
        else:
            # print Skipping if it is not a uuid
            print("Skipping")


# read manifest file
with open("../manifest.json", 'r+') as f:
    # read the file
    manifest = f.read()
    # parse the json
    manifest = json.loads(manifest)
    # get the gifs from the manifest
    gifs = manifest['gifs']
    # get the uuids from the gifs
    uuids = [gif['uuid'] for gif in gifs]
    # get the paths from the gifs
    paths = [gif['path'] for gif in gifs]
    # get the scripts from manifest
    scripts = manifest['scripts']
    # print the manifest
    # clean the gifs folder
    gifs, scripts = clean_gifs(paths, gifs, scripts)
    # set the gifs in the manifest to the new gifs
    manifest['gifs'] = gifs
    # save the scripts in the manifest to the new scripts
    manifest['scripts'] = scripts
    # write the manifest back to the file
    manifest = json.dumps(manifest, indent=4)
    # set the file pointer to the start of the file
    f.seek(0)
    # write the manifest back to the file
    f.write(manifest)
    # truncate the file
    f.truncate()

    

