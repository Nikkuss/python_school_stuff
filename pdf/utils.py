import json
import os
from uuid import uuid4

import requests
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import get_lexer_by_name


def py2html(code, options):
    """This code will convert a python code into an html code.

    This code will convert python code into html code. It will include
    the language, and the option to include any other
    information that you think is relevant.

    Args:
        code (str): The python code to be converted into an html code.
        options (dict): The options that can be used to customize the html
            code. These options can include the language, the line number,
            and any other information that you think is relevant.

    Returns:
        str: The html code that was converted from the python code.

    """
    formatter = HtmlFormatter(style='vs',
                              linenos=False,
                              noclasses=True,
                              cssclass='',
                              cssstyles='width:100vw',
                              prestyles='margin: 0')
    return highlight(code, get_lexer_by_name('python', **options), formatter)

# This code takes a python code and saves it as an html file

def save_py(code, path='../../scripts'):
    uuid = str(uuid4())
    path = os.path.join(path, uuid + '.html')
    with open(path, 'w') as f:
        f.write(py2html(code, {}))
    return { 'uuid': uuid }


# a function that takes python code as an input and calls "save_py(code)" to save the code as an html file and adds it to the manifest file
def py2html2(code, template=False):
    # open the manifest file
    with open('../../manifest.json', 'r') as f:
        manifest = json.load(f)
    # call "save_py(code)" to save the code as an html file
    py = save_py(code)
    # create html manifrst object
    html = {
        'uuid': py['uuid'],
        'path': f'scripts/{py["uuid"]}.html',
    }


    # add the uuid of the html file to the manifest file
    manifest['scripts'].append(html)
    # write the manifest file
    with open('../../manifest.json', 'w') as f:
        json.dump(manifest, f, indent=4)
    # return the uuid of the html file
    return py['uuid']


# a funtion that checks if a file exists on a server "https://gitlab.com/Nikkuss/python_school_stuff/-/raw/main/{file}" if it does return True else return False
def check_file(file):
    r = requests.get(f"https://gitlab.com/Nikkuss/python_school_stuff/-/raw/main/{file}")
    if r.status_code == 200:
        return True
    else:
        return False
    return False