# This code takes the first argument from the command line
# and uses it as a path. If the argument is a relative path,
# it converts it to an absolute path.

import sys
import os
import uuid
import json
import urllib3
import urllib
import utils
import jinja2
import pdfkit
import asyncio
from pyppeteer import launch


# get the first argument from the command line
source_script = sys.argv[1]
# get the second argument from the command line
source_rec = sys.argv[2]

# get the absolute path of the file
source_script_abs = os.path.abspath(source_script)
# get the absolute path of the file "source_rec"
source_rec_abs = os.path.abspath(source_rec)

print(f"Using {source_script_abs} as path for source script")
print(f"Using {source_rec_abs} as path for source recording")

# check if both "source_rec_abs" and "source_script_abs" exist and if not exit with an error and specify which file doesent exist
if not os.path.exists(source_rec_abs):
    print(f"{source_rec_abs} does not exist")
    exit(1)
if not os.path.exists(source_script_abs):
    print(f"{source_script_abs} does not exist")
    exit(1)
# generate hash from the source script and recording and use it as a uuid for the temporary directory
uuid = uuid.uuid5(uuid.NAMESPACE_DNS, f"{source_script_abs}{source_rec_abs}")

# generate random uuid
# uuid = uuid.uuid4()
# convert uuid to string
uuid = str(uuid)

exists = False

# check if the temporary directory already exists and if so delete it using shutil.rmtree
if os.path.exists(uuid) or os.path.exists(f"../gifs/{uuid}.gif"):
    print(f"Removing {uuid}")
    os.system(f"rm -rf {uuid}")
    exists = True





# create a temporary directory with the name of the uuid
os.mkdir(uuid)
# change the current working directory to the temporary directory
os.chdir(uuid)

print(f"Using {uuid} as temporary directory")
print(f"Using {uuid}.gif as temporary gif")
print(f"Using {uuid}.py as temporary script")

# get the last two folders of the path
source_script_remote_filename = '/'.join(source_script_abs.split('/')[-2:])

numtry = 0

while True:
    # check if the file is on gitlab using utils.check_file
    if not utils.check_file(source_script_remote_filename):
        # ask to user to upload the file to gitlab and give them a list of commands to run
        print("Please upload the file to gitlab")
        print("You can do this by running the following commands:")
        print("\"\"\"")
        print(f"git add {source_script_abs}")
        print(f"git commit -m \"Add {source_script_abs}\"")
        print("git push")
        print("\"\"\"")
        # ask the user to press enter when done   
        print("Press enter when done")
        # wait for the user to press enter
        input()
        # increment the number of tries
        numtry += 1
        # if the number of tries is greater than 5 exit with an error
        if numtry > 5:
            print("Too many tries")
            exit(1)
    else:
        # break the loop if the file is on gitlab
        break

if not exists:
    # call a command in the shell to convert the recording to a gif
    print("Converting to gif")
    os.system(f"agg {source_rec_abs} {uuid}.gif")

    # copy the source script to the temporary directory and give it a random name
    print("Copying script")
    os.system(f"cp {source_script_abs} {uuid}.py")

    # wait for 0.5 seconds to make sure the gif is done converting
    print("Waiting for 0.5 seconds")
    os.system("sleep 0.5")

    # copy the gif from the temporary directory to the parent directory and put it into a folder called "gifs"
    print("Copying gif")
    os.system(f"cp {uuid}.gif ../../gifs/{uuid}.gif")
else:
    print("Using existing gif")
# get the absolute path of the gif
gif_abs = os.path.abspath(f"../../gifs/{uuid}.gif")

# get the last two folders of gif_abs
gif_remote_filename = '/'.join(gif_abs.split('/')[-2:])



while True:
    # check if the file is on gitlab using utils.check_file
    if not utils.check_file(gif_remote_filename):
        # ask to user to upload the file to gitlab and give them a list of commands to run
        print("Please upload the file to gitlab")
        print("You can do this by running the following commands:")
        print("\"\"\"")
        print(f"git add {gif_abs}")
        print(f"git commit -m \"Add {gif_abs}\"")
        print("git push")
        print("\"\"\"")
        # ask the user to press enter when done   
        print("Press enter when done")
        # wait for the user to press enter
        input()
        # increment the number of tries
        numtry += 1
        # if the number of tries is greater than 5 exit with an error
        if numtry > 5:
            print("Too many tries")
            exit(1)
    else:
        # break the loop if the file is on gitlab
        break



# get the token from the environment variables
# print("Getting token")
# token = os.environ['TOKEN']


# this code will upload a gif to the nikkuss.com api
# datagen, headers2 = urllib3.encode_multipart_formdata({"file": (
#     f"{uuid}.gif", open(gif_abs, 'rb').read()), 
#     'allowedDownloads': '0', 
#     'expiryDays': '0'})
# headers = {
#     'apikey': token,
#     'Content-Type': headers2
# }

# print("Uploading gif")
# req = urllib.request.Request(url="https://files.nikkuss.com/api/files/add", method="POST", headers=headers, data=datagen)
# response = json.loads(req.data.decode("ascii"))
# # get the file id from the response
# file_id = response['FileInfo']['Id']
# hotlink_id = response['FileInfo']['HotlinkId']
# check if the response is not ok and if so exit with an error
# if response.json()['Result'] != 'OK':
#     print("Upload Failed exit.")
#     exit(1)
# print("Upload Successful")
if not exists:
    # add the gif to the manifest file in the parent directory "manifest.json"
    print("Adding gif to manifest")
    # open the file for reading and writing but not appending
    with open("../../manifest.json", "r+") as f:
        # read the file
        manifest = f.read()
        # parse the json
        manifest = json.loads(manifest)

        # gif manifest dictionary
        gif_manifest = {
            "uuid": uuid,
            "path": f"gifs/{uuid}.gif",
            # "file_id": file_id,
            # "hotlink_id": hotlink_id
        }
        # add the gif manifest to the manifest
        manifest['gifs'].append(gif_manifest)
        # convert the manifest back to a string with pretty print
        manifest = json.dumps(manifest, indent=4)
        # set the file pointer to the start of the file
        f.seek(0)
        # write the manifest back to the file
        f.write(manifest)
    print("Added gif to manifest")


async def screenshot(p, uuid):
    # use puppeteer to take a screenshot of the urls ["https://gitlab.com/Nikkuss/python_school_stuff/-/blob/main/{p}", "https://gitlab.com/Nikkuss/python_school_stuff/-/blob/main/gifs/{uuid}.gif" ]
    print("Taking screenshots")
    browser = await launch({'logLevel': 'DEBUG', 'headless': True, 'args': ['--no-sandbox']})
    page = await browser.newPage()
    await page.setViewport({'width': 1920, 'height': 1080})
    print("Going to urls")
    # replace - with /
    p = p.replace("-", "/")
    print(f"https://gitlab.com/Nikkuss/python_school_stuff/-/blob/main/{p}")
    await page.goto(f"https://gitlab.com/Nikkuss/python_school_stuff/-/blob/main/{p}")
    await page.waitFor(5000)
    await page.screenshot({'path': f'{uuid}-1.png'})
    print(f"https://gitlab.com/Nikkuss/python_school_stuff/-/blob/main/gifs/{uuid}.gif")
    await page.goto(f"https://gitlab.com/Nikkuss/python_school_stuff/-/blob/main/gifs/{uuid}.gif")
    await page.waitFor(5000)
    await page.screenshot({'path': f'{uuid}-2.png'})
    await browser.close()
    # show the screenshots to the user
    print("Showing screenshots")
    os.system(f"feh {uuid}-1.png {uuid}-2.png")

# open the file "source_script_abs" and read it
with open(source_script_abs) as f:
    contents = f.read()
    # convert the contents to html using "py2html2"
    print("Converting script to html")
    file = utils.py2html2(contents)
    # copy the html file to the parent directory
    print("Copying html")
    os.system(f"cp ../../scripts/{file}.html {uuid}.html")
    # copy template to the temporary directory
    print("Copying template")
    os.system(f"cp ../template.html template.html")
    #copy the css file to the temporary directory
    print("Copying css")
    os.system(f"cp ../style.css style.css")


    # load template using jinja2
    print("Loading template")
    template_loader = jinja2.FileSystemLoader('./')
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template("template.html")

    # get the last folder and file of "source_script_abs"
    p = source_script_abs
    # get the last folder and file of "source_script_abs"
    p = '/'.join(p.split('/')[-2:])


    # read the html file
    with open(f"{uuid}.html") as f:
        # read the html file
        html = f.read()
        # render the template
        print("Rendering template")
        html = template.render(html=html, gitfilepath=p, gitvideopath=f"gifs/{uuid}.gif")
        # html = template.render(html=html, gitfilepath=p, filesnikkusspath=f"https://files.nikkuss.com/hotlink/{hotlink_id}", gitvideopath=f"gifs/{uuid}.gif")
        # convert the html to pdf using "wkhtmltopdf"
        print("Converting html to pdf")
        config = pdfkit.configuration(wkhtmltopdf='/usr/bin/wkhtmltopdf')
        pdfkit.from_string(html, f"{uuid}.pdf", configuration=config, css='style.css')
        # replace "/" in the file name with "-"
        p = p.replace("/", "-")
        # copy the pdf to the parent directory
        print("Copying pdf")
        os.system(f"cp {uuid}.pdf ../{p}.pdf")

        # asyncio run the screenshot function and wait for it to finish
        asyncio.get_event_loop().run_until_complete(screenshot(p, uuid))




        



# delete the temporary directory
os.chdir("../")
os.system(f"rm -rf {uuid}")
