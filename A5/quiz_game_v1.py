from os import system, name, get_terminal_size
import time
import sys
import select
import threading
import curses


chars = ""


def map(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def clear():
    # for windows
    if name == 'nt':
        system('cls')
    # for mac and linux
    else:
        system('clear')

class _Getch: #  https://stackoverflow.com/questions/510357/how-to-read-a-single-character-from-the-user
    """Gets a single character from standard input.  Does not echo to the screen."""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()
class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, '', old_settings)
        return ch
class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


class InputThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.name = "InputThread"
    def run(self):
        global chars
        getch = _Getch()
        while True:
            chars += getch()



class Game():
    def __init__(self) -> None:
        self.width = get_terminal_size().columns
        # self.width = 10
        # self.height = 10
        self.height = get_terminal_size().lines
        self.level = 0
        self.difficulty = 1
        self.score = 0
        self.time_left = 100
        self.maxtime = 100
        self.answer = ""

        pass
    
    def header(self):
        bar = "-"*(self.width-2)
        joint = int((self.width-2)*0.75)
        jointbar = list(bar)
        jointbar[joint] = "#"
        jointbar = ''.join(jointbar)

        spacing = (self.width-11) - len(str(self.score))
        print(f"#{bar}#")
        print(f"| score: {self.score} {' '*spacing}|")
        print(f"#{jointbar}#")
        pass
    def footer(self):
        bar = "-"*(self.width-2)
        joint = int((self.width-2)*0.75)
        jointbar = list(bar)
        jointbar[joint] = "#"
        jointbar = ''.join(jointbar)
        progress = "#"* map(self.time_left,0,self.maxtime,0,len(bar))
        spacing = (self.width-2-len(progress))
        print(f"#{jointbar}#")
        print(f"|{progress}{' '*spacing}|")
        print(f"#{bar}#")
        pass

    def display(self):
        clear()
        self.header()
        joint = int((self.width-3)*0.75)
        joint_size_left = self.width-4-joint
        for i in range(self.height-7):
            data = ""
            if i == 3:
                data = f"Answer: {self.answer}"
            bar = " " * (joint - len(data))
            left = " " * joint_size_left
            print(f"| {data}{bar}|{left}|\n",end="")
        self.footer()
    def run(self):
        thread = InputThread()
        thread.start()
        
        while True:
            self.answer = chars
            self.score += 1 * (self.difficulty/2) 
            game.time_left -= 1
            time.sleep(0.1)
            self.display()
            if self.time_left <= 0:
                break
            

            



game = Game()
game.run()
    

    