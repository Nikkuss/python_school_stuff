import curses
from curses import ascii
import random
import math
def map(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False

class Game():
    def __init__(self, stdscr: curses.window) -> None:
        self.stdscr = stdscr
        self.score = 0
        self.health = 200
        self.maxtime = 100
        self.time = self.maxtime
        self.speed = 1
        self.dmg = 20
        self.difficulty = 1
        self.ans = ""
        self.question = ""
        self.gen_question()
        self.w = []
    def gen_question(self):
        w = [
            1,
            0.75,
            0.5,
            0.25
        ]
        self.w = w
        mn = 0
        if self.difficulty > 3.5:
            mn = int(self.difficulty * -5)

        nn = random.randrange(mn, int(self.difficulty * 5))
        mm = random.randrange(mn, int(self.difficulty * 5))
        op = random.choices(["+","-","*", "/"], weights=w, k=1)[0]


        if nn == 0 and op == "/": #handle div by 0
            nn += 1
        if mm == 0 and op == "/": #handle div by 0
            mm += 1 
        self.question = f"{nn} {op} {mm}"



    def display(self) -> None:
        height, width = self.stdscr.getmaxyx()
        self.stdscr.move(0,0)
        nt = int(width/10)


        for x in range(width):
            self.stdscr.addch("-")
        self.stdscr.move(1, 2)
        self.stdscr.addstr(f"Score: {self.score}")

        bar = map(self.health, 0, 200, 0, nt)

        self.stdscr.move(1, int(width / 2) - len(f"Diff: {self.difficulty}"))
        self.stdscr.addstr(f"Diff: {self.difficulty}")

        self.stdscr.move(1, int(width - nt - len("Health:  ")))
        self.stdscr.addstr(f"Health: ")
        for x in range(bar):
            self.stdscr.insch("#")


        self.stdscr.move(2,0)
        for x in range(width):
            self.stdscr.insch("-")

        self.stdscr.move(height-3,0)
        for x in range(width):
            self.stdscr.insch("-")

        if height > 12:
            for i, x in enumerate(self.w):
                self.stdscr.move(9+i,1)
                self.stdscr.insstr(f"\'{['+','-','*', '/'][i]}\': \'{x}\'")
                

        bar = map(self.time, 0, self.maxtime, 0, width-2)
        self.stdscr.move(height-2,1)
        for x in range(bar):
            self.stdscr.insch("#")
        self.stdscr.move(height-1,0)
        for x in range(width):
            self.stdscr.insch("-")
        self.stdscr.move(int(height/2),0)
        self.stdscr.move(5,0)
        self.stdscr.addstr(f"Question: {self.question}")
        self.stdscr.move(7,0)
        self.stdscr.addstr(f">> {self.ans}")
        self.stdscr.refresh()
    def mainloop(self) -> None:
        self.gen_question()
        while True:
            self.stdscr.clear()
            self.display()
            # self.stdscr.addstr(var)
            char = curses.initscr().getch()
            if ascii.isascii(char): # check if digit otherwise countdown
                self.ans += bytes([char]).decode("ascii")
                self.checkans()
            elif char == curses.KEY_BACKSPACE:
                if len(self.ans) > 0:
                    self.ans = self.ans[:-1]
            else:
                self.time -= self.speed
                if self.time < 1:
                    self.incorrect()
            if self.health <= 0:
                break
        height, width = self.stdscr.getmaxyx()
        self.stdscr.clear()
        text = "Game Over"
        self.stdscr.move(int(height/2), int((width - len(text))/2) )
        self.stdscr.addstr(text, curses.color_pair(1))
        text = f"Score: {self.score}, Difficulty: {self.difficulty}, Speed: {self.speed}"
        self.stdscr.move(int(height/2)+1, int((width - len(text))/2) )
        self.stdscr.addstr(text, curses.color_pair(1))
        self.stdscr.refresh()
        curses.nocbreak()
        self.stdscr.getkey()
    def checkans(self):
        if len(self.ans) > 0 and isfloat(self.ans):
            ans = round(float(self.ans),2)
            rans = round(float(eval(self.question)),2) # real ans
            if rans == ans:
                self.correct()
    def correct(self):
        self.stdscr.clear()
        self.display()
        curses.napms(250)
        self.difficulty += 0.1
        self.difficulty = round(float(self.difficulty),2)
        self.score += self.speed * 10 + self.difficulty
        self.score = round(float(self.score),2)
        if self.health < 100:
            self.health += int(self.health / 50)
        self.time = self.maxtime
        self.ans = ""
        self.gen_question()
        height, width = self.stdscr.getmaxyx()
        self.stdscr.clear()
        self.stdscr.move(int(height/2), int((width - len("Incorrect!"))/2) )
        self.stdscr.addstr("Correct!", curses.color_pair(2))
        self.stdscr.refresh()
        self.speed += 0.1
        self.speed = round(float(self.speed),2)
        curses.napms(500)
    def incorrect(self):
        height, width = self.stdscr.getmaxyx()
        self.stdscr.clear()
        self.stdscr.move(int(height/2), int((width - len(f"Incorrect. ans: {round(float(eval(self.question)),2)}!"))/2) )
        self.stdscr.addstr(f"Incorrect. ans: {round(float(eval(self.question)),2)}!", curses.color_pair(1))
        self.stdscr.refresh()
        curses.napms(1000)
        if self.difficulty > 2:
            self.difficulty -= 1
        self.difficulty = round(float(self.difficulty),2)
        self.health -= self.dmg * self.speed
        self.time = self.maxtime
        self.gen_question()
        self.ans = ""
        if self.speed > 0.2:
            self.speed -= 0.2
        if self.score > 0:
            self.score -= int(self.score / 10)
        self.score = round(float(self.score),2)
        

def main(stdscr):
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_WHITE)
    curses.halfdelay(1)
    curses.noecho()
    game = Game(stdscr)
    game.mainloop()
    
    pass


curses.wrapper( main )
