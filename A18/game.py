import pygame, sys
from pygame.locals import *
import time
pygame.font.init()
pygame.init()

#             R    G    B #
WHITE    = (255, 255, 255)#
BLUE     = (  0,   0, 255)#
RED      = (255,   0,   0)#
BLACK    = (  0,   0,   0)#
GOLD     = (255, 215,   0)#
HIGH     = (160, 190, 255)#
GREEN    = (  0, 255,   0)#
ORANGE   = (255, 165,   0)#

class Board:
    def __init__(self):
        self.fps = 60
        self.clock = pygame.time.Clock()
        self.turn = BLUE


        self.window_size = 600
        self.screen = pygame.display.set_mode((self.window_size, self.window_size))

        self.selected = None
        self.selected_pos = None
        self.square_size = self.window_size >> 3
        self.piece_size = self.square_size >> 3

        self.possible_moves = []
	
		# initialize squares and place them in matrix

        matrix = [[None] * 8 for i in range(8)]

        for x in range(8):
            for y in range(8):
                if (x % 2 != 0) and (y % 2 == 0):
                    matrix[y][x] = Tile(WHITE)
                elif (x % 2 != 0) and (y % 2 != 0):
                    matrix[y][x] = Tile(BLACK)
                elif (x % 2 == 0) and (y % 2 != 0):
                    matrix[y][x] = Tile(WHITE)
                elif (x % 2 == 0) and (y % 2 == 0): 
                    matrix[y][x] = Tile(BLACK)

        for x in range(8):
            for y in range(3):
                if matrix[x][y].color == BLACK:
                    matrix[x][y].occupant = Piece(RED)
            for y in range(5, 8):
                if matrix[x][y].color == BLACK:
                    matrix[x][y].occupant = Piece(BLUE)
        self.matrix = matrix
        
					
    def render(self):
        """
        Render the board.
        """
        # draw the board
        for x in range(8):
            for y in range(8):
                pygame.draw.rect(self.screen, self.matrix[x][y].color, (x * self.square_size, y * self.square_size, self.square_size, self.square_size))
                if self.matrix[x][y].occupant != None:
                    pygame.draw.circle(self.screen, self.matrix[x][y].occupant.color, (x * self.square_size + self.square_size / 2, y * self.square_size + self.square_size / 2), self.square_size / 2)

    def game_loop(self):
        """
        The main game loop.
        """
        while True:
            # count colors
            red = 0
            blue = 0
            for x in range(8):
                for y in range(8):
                    if self.matrix[x][y].occupant != None:
                        if self.matrix[x][y].occupant.color == RED:
                            red += 1
                        elif self.matrix[x][y].occupant.color == BLUE:
                            blue += 1
            if red == 0:
                # display blue wins in the middle of the screen uisng pygame.font
                font = pygame.font.SysFont('Arial', 100)
                text = font.render('Blue Wins!', True, BLUE)
                textRect = text.get_rect()
                textRect.center = (self.window_size // 2, self.window_size // 2)
                self.screen.blit(text, textRect)
                pygame.display.update()
                break
            elif blue == 0:
                # display red wins in the middle of the screen uisng pygame.font
                font = pygame.font.SysFont('Arial', 100)
                text = font.render('Red Wins!', True, RED)
                textRect = text.get_rect()
                textRect.center = (self.window_size // 2, self.window_size // 2)
                self.screen.blit(text, textRect)
                pygame.display.update()
                break

            
            mouse = pygame.mouse.get_pos()
            mouse_board = self.mouse_to_board(mouse)
            hovered = self.matrix[mouse_board[0]][mouse_board[1]].occupant
            self.clock.tick(self.fps)
            self.render()
            pygame.display.update()
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if hovered != None and self.turn == hovered.color :
                            self.selected = hovered
                            self.selected_pos = mouse_board
                            self.clear_moves()
                            if self.selected_pos != None:
                                self.possible_moves = self.valid_moves(self.selected, self.selected_pos, self.turn)
                                for move in self.possible_moves:
                                    if move[2] == False:
                                        self.matrix[move[0]][move[1]].occupant = Piece(GREEN)
                                    else:
                                        self.matrix[move[0]][move[1]].occupant = Piece(ORANGE)
                    elif hovered != None and hovered.color == GREEN:
                        self.clear_moves()
                        self.matrix[self.selected_pos[0]][self.selected_pos[1]].occupant = None
                        self.matrix[mouse_board[0]][mouse_board[1]].occupant = Piece(self.turn)
                        self.turn = BLUE if self.turn == RED else RED
                        self.selected = None
                        self.selected_pos = None
                    elif hovered != None and hovered.color == ORANGE:
                        self.clear_moves()
                        self.matrix[self.selected_pos[0]][self.selected_pos[1]].occupant = None
                        self.matrix[mouse_board[0]][mouse_board[1]].occupant = Piece(self.turn)
                        self.matrix[(self.selected_pos[0] + mouse_board[0]) // 2][(self.selected_pos[1] + mouse_board[1]) // 2].occupant = None
                        self.turn = BLUE if self.turn == RED else RED
                        self.selected = None
                        self.selected_pos = None
                    else:
                        self.clear_moves()
                        self.selected = None
                        self.selected_pos = None
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
        time.sleep(5)
        pygame.quit()
        sys.exit()
    def clear_moves(self):
        # remove all green squares
        for x in range(8):
            for y in range(8):
                if self.matrix[x][y].occupant != None and self.matrix[x][y].occupant.color == GREEN or self.matrix[x][y].occupant != None and self.matrix[x][y].occupant.color == ORANGE:
                    self.matrix[x][y].occupant = None

    def mouse_to_board(self, moise_pos: tuple):
        return (moise_pos[0] // self.square_size, moise_pos[1] // self.square_size)


    def valid_moves(self, piece, pos, color):
        """
        Given a piece and its position, returns a list of valid moves.
        """
        direction = 1 if color == RED else -1
        moves = []
        row = pos[0]
        col = pos[1]
        
        # get all tiles around the piece
        top_left = self.matrix[row - 1][col - 1] if row > 0 and col > 0 else None
        top_right = self.matrix[row - 1][col + 1] if row > 0 and col < 7 else None
        bottom_left = self.matrix[row + 1][col - 1] if row < 7 and col > 0 else None
        bottom_right = self.matrix[row + 1][col + 1] if row < 7 and col < 7 else None

        top_left_2 = self.matrix[row - 2][col - 2] if row > 1 and col > 1 else None
        top_right_2 = self.matrix[row - 2][col + 2] if row > 1 and col < 6 else None
        bottom_left_2 = self.matrix[row + 2][col - 2] if row < 6 and col > 1 else None
        bottom_right_2 = self.matrix[row + 2][col + 2] if row < 6 and col < 6 else None

        #
        if top_left_2 is not None and top_left_2.occupant is None and top_left is not None and top_left.occupant is not None and top_left.occupant.color != color and direction == -1:
            moves.append((row - 2, col - 2, True))
        if top_right_2 is not None and top_right_2.occupant is None and top_right is not None and top_right.occupant is not None and top_right.occupant.color != color and direction == 1:
            moves.append((row - 2, col + 2, True))
        if  bottom_left_2 is not None and bottom_left_2.occupant is None and bottom_left is not None and bottom_left.occupant is not None and bottom_left.occupant.color != color and direction == -1:
            moves.append((row + 2, col - 2, True))
        if bottom_right_2 is not None and bottom_right_2.occupant is None and bottom_right is not None and bottom_right.occupant is not None and bottom_right.occupant.color != color and direction == 1:
            moves.append((row + 2, col + 2, True))

        # check if the piece can move to the top left
        if top_left is not None and top_left.occupant is None and direction == -1:
            moves.append((row - 1, col - 1, False))
        # check if the piece can move to the top right
        if top_right is not None and top_right.occupant is None and direction == 1:
            moves.append((row - 1, col + 1, False))
        # check if the piece can move to the bottom left
        if bottom_left is not None and bottom_left.occupant is None and direction == -1:
            moves.append((row + 1, col - 1, False))
        # check if the piece can move to the bottom right
        if bottom_right is not None and bottom_right.occupant is None and direction == 1:
            moves.append((row + 1, col + 1, False))
        return moves

    def valid_move(self, piece, start, end):
        """
        Given a piece, its starting position, and its ending position, returns True if the move is valid, False otherwise.
        """
        # Check if the piece is moving in the correct direction
        if piece.color == RED:
            if end[0] < start[0]:
                return False
        elif piece.color == BLUE:
            if end[0] > start[0]:
                return False
        
        # Check if the move is diagonal
        if abs(end[0] - start[0]) != abs(end[1] - start[1]):
            return False
        
        # Check if the move is within the board boundaries
        if end[0] < 0 or end[0] > 7 or end[1] < 0 or end[1] > 7:
            return False
        
        # Check if the destination square is empty
        if self.matrix[end[0]][end[1]] is not None:
            return False
        
        # Check if the move is a capture
        if abs(end[0] - start[0]) == 2:
            # Calculate the position of the captured piece
            captured_row = (start[0] + end[0]) // 2
            captured_col = (start[1] + end[1]) // 2
            captured_piece = self.matrix[captured_row][captured_col]
            
            # Check if the captured piece belongs to the opposite player
            if captured_piece is None or captured_piece.color == piece.color:
                return False
        
        # All checks passed, the move is valid
        return True

class Piece:
	def __init__(self, color: tuple):
		self.color = color

class Tile:
	def __init__(self, color, occupant: Piece = None):
		self.color = color # color is either BLACK or WHITE
		self.occupant = occupant # occupant is a Square object
board = Board()
board.render()
board.game_loop()