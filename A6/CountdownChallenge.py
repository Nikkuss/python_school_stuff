from datetime import datetime
from dateutil.relativedelta import relativedelta
# Get Date from user

year = int(input('Enter a year: '))
month = int(input('Enter a month: '))
day = int(input('Enter a day: '))

# Create a date object from user input

userDate = datetime(year, month, day)

# Get current date and time

now = datetime.now()

# Get difference between dates

date = relativedelta(userDate,now)


# Print the date in the format of YYYYMMDD
print(f"{str(date.years).zfill(4)}::{str(date.months).zfill(2)}::{str(date.days).zfill(2)}")

# Print the year(s) of the date
print(f"Year(s): {date.years}")
# Print the month(s) of the date
print(f"Month(s): {date.months}")
# Print the day(s) of the date
print(f"Day(s): {date.days}")

# Print the date as a string
print(date)